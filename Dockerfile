FROM golang as build

RUN go get go.mongodb.org/mongo-driver/mongo

COPY . .

CMD ["go", "run", "src/main/main.go"]