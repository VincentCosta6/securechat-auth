package controllers

import (
	"../repos"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type LoginStruct struct {
	Username string
	Password string
}

func LoginRoute(c *gin.Context) {
	var form LoginStruct

	c.BindJSON(&form)

	foundUser, err := repos.GetUserByUsername(form.Username)

	if err != "" {
		c.JSON(400, gin.H{ "message": err })
		return
	}

	error := bcrypt.CompareHashAndPassword([]byte(foundUser.Password), []byte(form.Password))

	if error != nil {
		c.JSON(400, gin.H{ "message": "Password is incorrect" })
		return
	}

	/*tokenString, err := createJWTTokenString(foundUser)

	if err != nil {
		c.JSON(500, gin.H{"message": "Error creating JWT token", "err": err})
		return
	}*/

	c.JSON(200, gin.H{"message": "Login successful", "user": foundUser})
}