package controllers

import (
	"../repos"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type RegisterStruct struct {
	Username string
	Password string
	PublicKey string
}

func RegisterRoute(c *gin.Context) {
	var form RegisterStruct

	c.BindJSON(&form)

	if form.Username == "" {
		c.JSON(400, gin.H{"message": "You must send a username"})
		return
	}

	if form.Password == "" {
		c.JSON(400, gin.H{"message": "You must send a password"})
		return
	}

	if form.PublicKey == "" {
		c.JSON(400, gin.H{"message": "You must send a public key"})
		return
	}

	if repos.UsernameExists(form.Username) {
		c.JSON(400, gin.H{"message": "Username already exists", "username": "Username taken"})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(form.Password), 17)

	if err != nil {
		c.JSON(400, gin.H{ "message": "Fatal error hashing password", "err": err })
		return
	}

	var hashedPassword = string(hash)

	_, err = repos.InsertUser(form.Username, hashedPassword, form.PublicKey)

	if err != nil {
		c.JSON(400, gin.H{ "message": "Error inserting user into db", "err": err })
		return
	}

	c.JSON(200, gin.H{"message": "Account created successfully"})
}