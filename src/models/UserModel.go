package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserModel struct {
	ID primitive.ObjectID `bson:"_id" json:"_id,omitempty"`
	Username string `json:"username""`
	Password string `json:"password""`
	PublicKey string `json:"publicKey""`
}