package repos

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"../models"
)

var (
	Users *mongo.Collection
)

func GetUserByUsername(username string) (user models.UserModel, error string) {
	var foundUser models.UserModel

	err := Users.FindOne(context.TODO(), bson.D{{"username", username}}).Decode(&foundUser)

	if err != nil {
		return  foundUser, "Username not found"
	}

	return foundUser, ""
}

func UsernameExists(username string) bool {
	_, err := GetUserByUsername(username)

	return err == ""
}

func InsertUser(username string, hashedPassword string, publicKey string) (mongo.InsertOneResult, error) {
	user := models.UserModel{ID: primitive.NewObjectID(), Username: username, Password: hashedPassword, PublicKey: publicKey}

	return InsertUserModel(user)
}

func InsertUserModel(user models.UserModel) (mongo.InsertOneResult, error) {
	res, err := Users.InsertOne(context.TODO(), user)

	return *res, err
}