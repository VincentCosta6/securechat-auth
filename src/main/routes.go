package main

import (
	"../controllers"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"os"
)

func InitializeMiscRoutes(r *gin.Engine) {
	pingStatus := bson.M{"msg": "healthy"}

	r.GET("/ping", func (c *gin.Context) {
		c.JSON(200, pingStatus)
	})
}

func InitializeMainRoutes(r *gin.Engine) {
	r.POST("/register", controllers.RegisterRoute)
	r.POST("/login", controllers.LoginRoute)
	r.POST("/refresh")

	r.RunTLS(":443", os.Getenv("AUTH_PUBLIC_KEY"), os.Getenv("AUTH_PRIVATE_KEY"))
}