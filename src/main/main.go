package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"../repos"
)

func main() {
	setupENV()
	connectToDB()
	setupRoutes()
}

func setupENV() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(err)
		log.Fatal("Error loading .env file")
		os.Exit(1)
	}

	checkENVS := [...]string {
		"MONGO_HOST",
		"MONGO_PORT",
		"MONGO_USERNAME",
		"MONGO_PASSWORD",
		"MONGO_DB",
		"AUTH_PUBLIC_KEY",
		"AUTH_PRIVATE_KEY",
	}

	for _, elem := range checkENVS {
		if os.Getenv(elem) == "" {
			log.Fatal("The env variable: " + elem + " was empty")
			os.Exit(1)
		}
	}
}

func connectToDB() {
	mongoURI := os.Getenv("MONGO_HOST")
	mongoPort := os.Getenv("MONGO_PORT")

	mongoUsername := os.Getenv("MONGO_USERNAME")
	mongoPassword := os.Getenv("MONGO_PASSWORD")
	clientOptions := options.Client().ApplyURI("mongodb://" + mongoUsername + ":" + mongoPassword + "@" + mongoURI + ":" + mongoPort + "/?authSource=admin&ssl=false")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	fmt.Println("Connected to MongoDB!")

	dbName := os.Getenv("MONGO_DB")

	repos.Users = client.Database(dbName).Collection("users")
}

func setupRoutes() {
	//gin.SetMode(gin.ReleaseMode)

	r := gin.Default()
	r.Use(CORSMiddleware())

	InitializeMiscRoutes(r)
	InitializeMainRoutes(r)

	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan struct{})

	fmt.Println("Waiting for interrupt")

	signal.Notify(signalChan, os.Interrupt)
	go func() {
		<-signalChan
		fmt.Println("\nReceived an interrupt, stopping services...\n")
		close(cleanupDone)
	}()
	<-cleanupDone
}

// TODO: Move middleware
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With, token")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}